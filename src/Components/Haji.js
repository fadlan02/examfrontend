import React, { Component, Fragment } from 'react'
import '../App.css';
import Header from './Header';
import Footer from './Footer';
class Haji extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nik: '',
            nama: '',
            ttl: '',
            jk: '',
            alamat: '',
            rt: '',
            rw: '',
            kelurahan: '',
            kecamatan: '',
            agama: '',
            status: '',
            pekerjaan: '',
            kewarganegaraan: ''
        }
    }

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
        console.log(event.target.value);
    }
    handleSubmit = (event) => {
        event.preventDefault();
        alert("hasil : " + this.state.nama + this.state.ttl)
    }

    render() {
        return (
            <Fragment>
                <Header />
                <div className="card">
                    <form onSubmit={this.handleSubmit}>
                        <h1 className="titleCard">Data KTP</h1>
                        <label>
                            NIK
                        </label><br />
                        <input type="number" name="nik" value={this.state.nik} onChange={this.handleChange} /><br />
                        <label>
                            Nama
                        </label><br />
                        <input type="text" name="nama" value={this.state.nama} onChange={this.handleChange} /><br />
                        <label>
                            Tempat, Tanggal Lahir
                        </label><br />
                        <input type="date" name="ttl" value={this.state.ttl} onChange={this.handleChange} /><br />
                        <label>
                            Jenis Kelamin
                        </label><br />
                        <input type="text" name="jk" value={this.state.jk} onChange={this.handleChange} /><br />
                        <label>
                            Alamat
                        </label><br />
                        <input type="text" name="alamat" value={this.state.alamat} onChange={this.handleChange} /><br />
                        <label>
                            RT
                        </label><br />
                        <input type="number" name="rt" value={this.state.rt} onChange={this.handleChange} /><br />
                        <label>
                            RW
                        </label><br />
                        <input type="number" name="rw" value={this.state.rw} onChange={this.handleChange} /><br />
                        <label>
                            Kelurahan
                        </label><br />
                        <input type="text" name="kelurahan" onChange={this.handleChange} value={this.state.kelurahan} /><br />
                        <label>
                            Kecamatan
                        </label><br />
                        <input type="text" name="kecamatan" onChange={this.handleChange} value={this.state.kecamatan} /><br />
                        <label>
                            Agama
                        </label><br />
                        <input type="text" name="agama" onChange={this.handleChange} value={this.state.agama} /><br />
                        <label>
                            Status
                        </label><br />
                        <input type="text" name="status" onChange={this.handleChange} value={this.state.status} /><br />
                        <label>
                            Kewarganegaraan
                        </label><br />
                        <input type="text" name="kewarganegaraan" onChange={this.handleChange} value={this.state.kewarganegaraan} /><br />
                        <button type="submit">Kirim</button>
                    </form>
                </div>
                <div className="cardHasilKTP">
                    <ul>
                        <li>{this.state.nik}</li>
                        <li>{this.state.nama}</li>
                        <li>{this.state.ttl}</li>
                        <li>{this.state.jk}</li>
                        <li>{this.state.alamat}</li>
                        <li>{this.state.rt}</li>
                        <li>{this.state.rw}</li>
                        <li>{this.state.kelurahan}</li>
                        <li>{this.state.kecamatan}</li>
                        <li>{this.state.agama}</li>
                        <li>{this.state.status}</li>
                        <li>{this.state.pekerjaan}</li>
                        <li>{this.state.kewarganegaraan}</li>

                    </ul>
                </div>
                <Footer />
            </Fragment >
        );
    }
}

export default Haji;