import React, { Component, Fragment } from 'react';
import Header from './Header';
import Footer from './Footer';
class Jadwal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            tanggalKeberangkatan: '',
            tanggalKepulangan: '',
            noKeberangkatan: ''
        }
    }
    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
        console.log(event.target.value);
    }
    handleSubmit = (event) => {
        event.preventDefault();
        alert("hasil : " + this.state.id)
    }


    render() {
        return (
            <Fragment>
                <Header />
                <div className="card">
                    <h1 className="titleCard">Jadwal Haji</h1>
                    <form onSubmit={this.handleSubmit}>
                        <label>
                            ID Haji
                    </label><br />
                        <input type="text" name="id" value={this.state.id} onChange={this.handleChange} /><br />
                        <label>
                            Tanggal Keberangkatan
                    </label><br />
                        <input type="date" name="tanggalKeberangkatan" value={this.state.tanggalKeberangkatan} onChange={this.handleChange} /><br />
                        <label>
                            Tanggal Kepulangan
                        </label><br />
                        <input type="date" name="tanggalKepulangan" value={this.state.tanggalKepulangan} onChange={this.handleChange} /><br />
                        <label>
                            Nomor Keberangkatan
                    </label><br />
                        <input type="number" name="noKeberangkatan" value={this.state.noKeberangkatan} onChange={this.handleChange} /><br />
                        <button type="submit" value="Submit">Kirim</button>
                    </form>
                </div>
                <Footer />
            </Fragment>
        );
    }
}

export default Jadwal;