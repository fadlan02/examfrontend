import React, { Component, Fragment } from 'react';
import '../App.css';

class Header extends Component {
    state = {}
    render() {
        return (
            <Fragment>
                <div className="header">
                    <h1 className="title">YukHaji</h1>
                </div>
            </Fragment>
        );
    }
}

export default Header;